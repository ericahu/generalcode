/**
 *  Use Knuth-Morris-Pratt string matching algorithm
 */
import java.util.*;

public class palindromicBorder {

  public static int match(String w, String s) {
    int m = 0; // beginning of current match in s
    int i = 0; // position of current character in w
    int[] T = new int[s.length()];
    // Generate table
    int pos = 2, cnd = 0;
    T[0] = -1;
    T[1] = 0;
    while (pos < w.length()) {
      if (w.charAt(pos-1) == w.charAt(cnd)) {
        T[pos] = cnd + 1;
        cnd += 1;
        pos += 1;
      }
      else if (cnd > 0) {
        cnd = T[cnd];
      }
      else {
        T[pos] = 0;
        pos += 1;
      }
    }

    // BEGIN ALGORITHM
    while (m+1 < s.length()) {
      if (w.charAt(i) == s.charAt(m+i)) {
        if (i == w.length()-1) {
          return 1;
        }
        i++;
      }
      else {
        if (T[i] > -1) {
          m = m+i-T[i];
          i = T[i];
        }
        else {
          m++;
          i = 0;
        }
      }
    }
    return 0;
  }

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    String input = scan.nextLine();

    int n = input.length();
    int mid = n/2+1;
    if (n%2 == 1) {
      mid++;
    }
    long ans = -n;

    for (int i = 1; i <= mid; i++) {    // for every substring length
      for (int j = 0; j < n-2*i+2; j++) { // for every substring position
        String s = input.substring(j,j+i);
        String w = input.substring(j+i-1,n);
        System.out.println(s+"..."+w);

        ans += match(w, s);
      }
    }
    System.out.println(ans);
  }
}
