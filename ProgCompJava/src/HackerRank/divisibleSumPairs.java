package HackerRank;

/*
 * [PASSED ALL TESTS]
 * https://www.hackerrank.com/challenges/divisible-sum-pairs
 */

import java.util.*;
public class divisibleSumPairs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = scan.nextInt();
        int [] num = new int[n];
        scan.nextLine();

        int pairs = 0;
        for (int j = 0; j < n; j++) {
            num[j] = scan.nextInt();
            if (j == 0) {
                continue;
            }
            for (int i = 0; i < j; i++) {
                if ((num[i]+num[j])%k == 0) {
                    pairs++;
                }
            }
        }
        System.out.println(pairs);
    }
}
