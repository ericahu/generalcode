package HackerRank;
import java.util.*;

/**
 * [INCOMPLETE]
 * https://www.hackerrank.com/challenges/palindromic-border
 */
public class palindromicBorder {

    public static int palindromeDepth (String s) {
        int n = s.length(), m = s.length()/2;
        int ans = 0;
        for (int i = 0; i < m; i++) {
            if (s.charAt(i) != s.charAt(n-i-1)) {
                return ans;
            }
            ans++;
        }
        return ans;
    }

    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);
//        String input = scan.nextLine();
        String input = "abcacb";
        int n = input.length();
        long ans = 0;

        for (int i = 2; i <= n; i++) {
            for (int j = 0; j <= n-i; j++) {
                String s = input.substring(j,j+i);
                System.out.println(s+"..."+palindromeDepth(s));
                ans += palindromeDepth(s);
            }
        }

        System.out.println(ans);
    }
}
