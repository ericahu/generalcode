package HackerRank;
import java.util.*;

/**
 * [PASSED ALL TESTS]
 * https://www.hackerrank.com/challenges/reduced-string
 */
public class superReducedString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        int i = 0, n = input.length();

        while (i < n-1) {
            if (input.length() == 1) {
                System.out.println(input);
                return;
            }
            if (input.length() == 0) {
                System.out.println("Empty String");
                return;
            }
            char a = input.charAt(i), b = input.charAt(i+1);
            if (a == b) {
                String c = Character.toString(a).concat(Character.toString(b));
                input = input.replaceAll(c, "");
                i = 0;
                n = input.length();
                continue;
            }
            else {
                i++;
                continue;
            }
        }
        if (input.length()==0) {
            System.out.println("Empty String");
            return;
        }
        System.out.println(input);
    }
}
