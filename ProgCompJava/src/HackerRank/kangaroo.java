package HackerRank;
/**
 * [PASSED ALL TESTS]
 * https://www.hackerrank.com/challenges/kangaroo
 */
import java.util.*;
public class kangaroo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int x1 = scan.nextInt();
        int v1 = scan.nextInt();
        int x2 = scan.nextInt();
        int v2 = scan.nextInt();

        if ((x1 == x2 && v1 != v2) || (x1 != x2 && v1 == v2) || (x1 < x2 && v1 < v2)) {
            System.out.println("NO");
            return;
        }

        // Let kangaroo 1 always be in front
        if (x1 < x2) {
            int temp = x1;
            x1 = x2;
            x2 = temp;
            temp = v1;
            v1 = v2;
            v2 = temp;
        }
        while (x1 > x2) {
            x1 += v1;
            x2 += v2;
        }
        if (x1 == x2) {
            System.out.println("YES");
            return;
        }
        System.out.println("NO");
    }
}
