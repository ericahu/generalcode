package Algorithms;
import java.util.*;

/**
 * Knuth-Morris-Pratt string matching
 */
public class KMP {
    public static int match(String w, String s) {
        if (w.length() == 0 || w.length() > s.length()) {
            return -1;
        }
        if (w.length() == 1) {
            if (s.contains(w)) {
                return s.indexOf(w);
            }
            return -1;
        }
        int[] table = partialMatchTable(w);
        int sp = 0, wp = 0;
        while (sp < s.length()) {
            if (s.charAt(sp+wp) == w.charAt(wp)) {
                wp++;
                if (wp == w.length()) {
                    return sp;
                }
                continue;
            }
            sp = sp + wp - table[wp];
            if (wp > 0) {
                wp = table[wp];
            }
        }
        return -1;
    }
    public static int[] partialMatchTable(String w) {
        int[] table = new int[w.length()];
        table[0] = -1;
        table[1] = 0;
        int lastMatch = 0;
        int p = 2;
        while (p < table.length) {
            if (w.charAt(p-1) == w.charAt(lastMatch)) {
                lastMatch++;
                table[p] = lastMatch;
                p++;
            }
            else if (lastMatch != 0) {
                lastMatch = table[lastMatch];
            }
            else {
                table[p] = lastMatch;
                p++;
            }
        }
        return table;
    }
    public static void main (String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the word you are search for:");
        String w = scan.nextLine();
        System.out.println("Enter the text you want to search from:");
        String s = scan.nextLine();
        System.out.println(match(w,s));
    }
}
